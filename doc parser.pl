# This is a document parser.

#! /usr/local/bin/perl -w
use strict;

## Ask for space file name
print "Please enter the file name of your fasta sequence file?\n";
my $fasta_name=<STDIN>;
chomp($fasta_name);
# print "The file name is". $fasta_name. "\n";
## Declare variable and open file
my $seq_counter=-1;
my @seq=();
my @title=();
# Read $fasta_name file
open (INPUT,"<$fasta_name");
while (my $line=<INPUT>) {
  chomp($line);    # get rid of non-printable enter key
  $line=~s/\r$//;  # get rid of window-based non-printable keys
  if ($line=~m/>/) {
     # when the line contains '>' symbol, counter++ and store inside @title
     $seq_counter++;
     $title[$seq_counter]=$line;
  }
  else {
     # otherwise, do string concatenation and store inside @seq
      $seq[$seq_counter]=$seq[$seq_counter].$line;
  }
} 
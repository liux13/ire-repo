#!/usr/bin/perl -w
use strict;
$/=">";

my @tmp;
my @seq_sum;
my $t_p;
my $ID;
my $s_seq;
my @seq_tmp;
my $sub_s_seq;
my $revs;
my $k;
my $backward_s_seq;
my $forward_s_seq;
my @for_s_seq;
my @back_s_seq;
my $posi;
my @endpo;
my $sub_1;
my $sub_2;
my $sub_3;

open IN, "<$ARGV[0]" or die "$!";
<IN>;
while(<IN>){
	chomp;
	@tmp=split(/\n/,$_);
	for(@tmp){
		$t_p.=$_."\t";	
		}
	push @seq_sum, $t_p;
	@tmp=();
	$t_p="";
	}

for(@seq_sum){
        @seq_tmp=split(/\s+/,$_);       
        $ID=shift(@seq_tmp);
        
        for(@seq_tmp){
                $s_seq.=$_;
                }
#       print "$ID<br>$s_seq<br>";  
while($s_seq=~/CAGUG[ACGT]/g){
        $posi= pos $s_seq;
        push (@endpo,$posi);
        $posi=""; 
        }

#get the results
print ">$ID\n";
if(@endpo){
        for(@endpo){

        #5' arm 
        $forward_s_seq=substr($s_seq,$_-6-12,12);

        #3' arm
        $backward_s_seq=substr($s_seq,$_,12);

        #reverse the 5' arm for pair-detection
        $revs=reverse $forward_s_seq;
        $revs=~tr/AGTC/TCAG/;

        #save the s_sequences into two array    
        @for_s_seq=split(//,$revs);
        @back_s_seq=split(//,$backward_s_seq);

        #get the arm length "$k"                
        for(my $i=0;$i<=11;$i++){
                if($for_s_seq[$i]=~/$back_s_seq[$i]/){

                        $k++;
                }else{
                        last;# jump out of the loop
                        }
                }

        #get the sub-s_sequence
        $sub_s_seq=substr($s_seq,$_-$k-6,2*$k+6);

        #print the sub-s_sequence
        
        #print "$sub_s_seq<br>";
        $sub_1=substr($sub_s_seq,0,$k);
        $sub_2=substr($sub_s_seq,$k,6);
        $sub_3=substr($sub_s_seq,$k+6,$k);
        
        print "$sub_1";
        print "$sub_2";
        print "$sub_3\n";

        
        #print the match structure 
        print "("x$k;
        print " "x6;
        print ")"x$k;
        print "\n";

        #print the spacer length and the arm length
       # print "Spacer Length=6\n";
        print "6\n";
        unless($k==0){
        #print "Arm Length=$k\n";
        print "$k\n";
                }else{
        #print "Arm Length=0\n"; 
        print "0\n"; 
                }       
        #$sub_s_seq="";
        $sub_1="";
        $sub_2="";
        $sub_3="";
        $k=0;
        $revs="";
        $backward_s_seq="";
        $forward_s_seq="";
        @for_s_seq=();
        @back_s_seq=();
        }
}else{
        print "No IRE has been found!<br>";
}
        
        $ID="";
        $s_seq="";
        @seq_tmp=();
        @endpo=();
        
}
@seq_sum=();

**Requirements**
[1]. Document Parser (done)
[2.2] Identification of iron responsive element (IRE). (done)
[3]. Database
You will design your own database (using a minimum of one table) to hold your data. You 
need to create a file named “final_proj.sql” as the DDL SQL for your database. Then, you 
are required to populate your data for your table(s) using your Perl script “final_proj.pl”. 
In this “final_proj.pl”, you can generate DML SQL statement and insert data into the 
table. Alternatively, you can generate a tab-delimited output file for each individual table, 
which can be dumped easily through mysql import program (example will be provided later). 
You will be provided with a task-specific sequence file, which will be parsed by your Perl 
script “final_proj.pl”. For the query sequence entered online by users, your program does 
not need to save them into the database. In all cases, you are required to develop a web 
interface that allows users to query your database to see the result using sequence name. 
Also, your another interface will allow a user to enter his/her single sequence for analysis. 
[4] Development of a web-based query tool configured to allow users to query the 
database by sequence name. Development of a web-based interface to allow a user to analyze his/her own data
(one single sequence) on the fly.

**Rubrics for Grading:**
      Project teams have been chosen to pair two students, giving you a 
chance to work together closely. We understand some students have more coding 
capability than others. What is important to us is that both team members should put 
good efforts to project design, implementation, test and report. Division of labor is up to 
you, though keep in mind that your final grade will consider the peer evaluation from your 
team members. Codes need to be easy to follow with clean and simple design, with
different functions. Codes need to have pseudo codes and comments, with proper 
indentation that will facilitate debugging and code readability. Codes that do not follow 
conventional usages of variables (array, hash, file handler, scalar variable, hash reference, 
array reference and so on) will incur some point detections. Within 2-page project report, 
you need to describe your system design and implementation, highlight your functions and 
describe its usability. 

**Due Date**: 
        Wednesday, Dec 10 2014, at 5:00 pm. Your group should turn in: 
                           (1) Source codes 
                           (2) 2-page project report 
                           (3) An email to TA that indicates peer evaluation of your team members
**Submission:**
     Your submission must include two Perl CGI files5:
     (1) “final_MiamiUserName_1.pl”: This interface allows users to query your database 
through sequence name.
     (2) “final_MiamiUserName_2.pl”: This interface allows users to enter their 
sequences and conduct instant sequence analysis.
Here, MiamiUserName should be the first part before @ in one group member’s 
email address. 

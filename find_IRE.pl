#/usr/bin/perl -w
use strict;

#define varibles;
	my $sub_seq;
	my $revs;
	my $k;
	my $backward_seq;
	my $forward_seq;
	my @for_seq;
	my @back_seq;
	my $posi;
	my @endpo;

#test sequence
my $seq="CAGCTAGCTACCCCCCCCCCACAGUGTTGGGGGGGGGGGGCTCTTTTTTTTTTTTGTTGCAGUGTCAAAAAAAAAAAAA";

#get the position of each matched "CAGUGN" by using function <pos>
while($seq=~/CAGUG[ACGT]/g){
	$posi= pos $seq;
	push (@endpo,$posi);
	$posi=""; 
	}

#get the results
for(@endpo){

	#5' arm 
	$forward_seq=substr($seq,$_-6-12,12);

	#3' arm
	$backward_seq=substr($seq,$_,12);

	#reverse the 5' arm for pair-detection
	$revs=reverse $forward_seq;
	$revs=~tr/AGTC/TCAG/;

	#save the sequences into two array	
	@for_seq=split(//,$revs);
	@back_seq=split(//,$backward_seq);

	#get the arm length "$k"		
	for(my $i=0;$i<=11;$i++){
		if($for_seq[$i]=~/$back_seq[$i]/){

			#print "I am so fucking awesome!\n";
			$k++;
		}else{
			last;# jump out of the loop
			}
		}

	#get the sub-sequence
	$sub_seq=substr($seq,$_-$k-6,2*$k+6);

	#print the sub-sequence
	print "$sub_seq\t";

	#print the match structure 
	print "("x$k;
	print " "x6;
	print ")"x$k;
	print "\t";

	#print the spacer length and the arm length
	print "6\t";
	print "$k\n";
		
	$sub_seq="";
	$k=0;
	$revs="";
	$k=0;
	$backward_seq="";
	$forward_seq="";
	@for_seq=();
	@back_seq=();
	}